from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('search', views.search, name='search'),

    #   VIEWING
    path('movie_list/', views.movie_list, name='movie_list'),
    path('director_list/', views.director_list, name='director_list'),
    path('details/movie/', views.movie_detail),
    path('details/director/', views.director_detail),

    #   EDITING
    path('edit/movie/', views.movie_edit),
    path('new/movie/', views.movie_create, name='movie_create'),
    path('remove/movie/', views.movie_remove),
    path('movie_perform/', views.movie_perform, name='movie_perform'),

    path('edit/director/', views.director_edit),
    path('new/director/', views.director_create, name='director_create'),
    path('remove/director/', views.director_remove),
    path('director_perform/', views.director_perform, name='director_perform'),

    #   AUTHENTICATION
    path('login/', views.login_form, name='login'),
    path('logout/', views.logout_form, name='logout'),
    path('auth/', views.auth, name='auth')
]
