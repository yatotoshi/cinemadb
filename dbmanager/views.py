from django.shortcuts import render
from .models import Movie, Director
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect


def home(request):
    return render(request, 'dbmanager/main.html')


def search(request):
    query = request.GET.get('query', None)
    if query is None:
        return redirect('/')

    movies = Movie.objects.filter(title__icontains=query)
    directors = Director.objects.filter(name__icontains=query)
    is_admin = request.user.groups.filter(name='administrator').exists()
    template_dict = {
        'is_admin': is_admin,
        'movies': movies,
        'directors': directors
    }
    return render(request, 'dbmanager/search.html', template_dict)


def movie_list(request):
    movies = Movie.objects.all()
    is_admin = request.user.groups.filter(name='administrator').exists()
    template_dict = {
        'is_admin': is_admin,
        'movies': movies
    }
    return render(request, 'dbmanager/movie_list.html', template_dict)


def director_list(request):
    directors = Director.objects.all()
    is_admin = request.user.groups.filter(name='administrator').exists()
    template_dict = {
        'is_admin': is_admin,
        'directors': directors
    }
    return render(request, 'dbmanager/director_list.html', template_dict)


def movie_detail(request):
    id = request.GET.get('id', None)
    if id is None:
        return redirect('/movie_list')
    movie = Movie.objects.get(id=id)
    template_dict = {
        'movie': movie,
        'directors': movie.directors.all()
    }
    return render(request, 'dbmanager/movie_details.html', template_dict)


def director_detail(request):
    id = request.GET.get('id', None)
    if id is None:
        return redirect('/movie_list')
    director = Director.objects.get(id=id)
    is_admin = request.user.groups.filter(name='administrators').exists()
    template_dict = {
        'is_admin': is_admin,
        'director': director
    }
    return render(request, 'dbmanager/director_details.html', template_dict)


def admin_required(func):
    def wrapper(*args, **kwargs):
        is_admin = args[0].user.groups.filter(name='administrator').exists()
        if not is_admin:
            return None
        return func(*args, **kwargs)
    return wrapper


@admin_required
def movie_perform(request):
    data = request.POST
    if 'action' not in request.POST:
        return redirect('/')

    keys = list(data.keys())
    directors_ids = []
    prefix = 'director_'
    for key in keys:
        if prefix in key:
            directors_ids.append(key[len(prefix):])

    if data['action'] == 'update' and all([el in data for el in ('id', 'title', 'picture_url', 'slogan', 'year')]):
        movie = Movie.objects.get(id=data['id'])
        movie.title = data['title']
        movie.picture_url = data['picture_url']
        movie.slogan = data['slogan']
        movie.year = data['year']

        if len(directors_ids) != 0:
            movie.directors.clear()
            for director_id in directors_ids:
                director = Director.objects.get(id=director_id)
                movie.directors.add(director)
        movie.save()

    elif data['action'] == 'create' and all([el in data for el in ('title', 'picture_url', 'slogan', 'year')]):
        movie = Movie.objects.create(title=data['title'], picture_url=data['picture_url'],
                                            slogan=data['slogan'], year=data['year'])
        movie = Movie.objects.get(id=movie.id)
        if len(directors_ids) != 0:
            movie.directors.clear()
            for director_id in directors_ids:
                director = Director.objects.get(id=director_id)
                movie.directors.add(director)
        movie.save()
    return redirect('/movie_list/')


@admin_required
def movie_edit(request):
    id = request.GET.get('id', '')
    movie = Movie.objects.get(id=id)
    director_list = Director.objects.all()
    directors_to_check = movie.directors.all()
    template_dict = {
        'movie': movie,
        'directors': director_list,
        'directors_to_check': directors_to_check,
        'action': 'update'
    }
    return render(request, 'dbmanager/movie_edit.html', template_dict)


@admin_required
def movie_create(request):
    movie = {'id': None, 'title': '', 'picture_url': '', 'slogan': '', 'year': '', 'directors': []}
    director_list = Director.objects.all()
    template_dict = {
        'movie': movie,
        'directors': director_list,
        'directors_to_check': [],
        'action': 'create'
    }
    return render(request, 'dbmanager/movie_edit.html', template_dict)


@admin_required
def movie_remove(request):
    id = request.GET.get('id', '')
    Movie.objects.get(id=id).delete()
    return redirect('/movie_list')


@admin_required
def director_perform(request):
    data = request.POST
    if 'action' not in request.POST:
        return redirect('/')

    if data['action'] == 'update' and all([el in data for el in ('id', 'name', 'picture_url', 'country')]):
        director = Director.objects.get(id=data['id'])
        director.name = data['name']
        director.picture_url = data['picture_url']
        director.country = data['country']
        director.save()

    elif data['action'] == 'create' and all([el in data for el in ('name', 'picture_url', 'country')]):
        director = Director.objects.create(name=data['name'], picture_url=data['picture_url'],
                                           country=data['country'])
        director = Director.objects.get(id=director.id)
        director.save()

    return redirect('/director_list/')


@admin_required
def director_edit(request):
    id = request.GET.get('id', '')
    director = Director.objects.get(id=id)
    template_dict = {
        'director': director,
        'action': 'update'
    }
    return render(request, 'dbmanager/director_edit.html', template_dict)


@admin_required
def director_create(request):
    director = {'id': None, 'name': '', 'picture_url': '', 'country': ''}
    template_dict = {
        'director': director,
        'action': 'create'
    }
    return render(request, 'dbmanager/director_edit.html', template_dict)


@admin_required
def director_remove(request):
    id = request.GET.get('id', '')
    Director.objects.get(id=id).delete()
    return redirect('/director_list')


def login_form(request):
    return render(request, 'dbmanager/auth.html')


def logout_form(request):
    logout(request)
    return redirect('/')


def auth(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('/')
    else:
        return render(request, 'dbmanager/auth.html', {'status': 'Неверный логин или пароль!'})
