from django.db import models


class Director(models.Model):
    name = models.CharField('Имя', max_length=255)
    country = models.CharField('Страна', max_length=64)
    picture_url = models.CharField('Картинка', max_length=2048, default='None')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Режиссер'
        verbose_name_plural = 'Режиссеры'


class Movie(models.Model):
    title = models.CharField('Название', max_length=255)
    year = models.IntegerField('Год')
    directors = models.ManyToManyField(Director)
    slogan = models.TextField('Слоган', default='--')
    picture_url = models.CharField('Картинка', max_length=2048, default='None')

    def __str__(self):
        return f'{self.title} [{self.year}]'

    class Meta:
        verbose_name = 'Фильм'
        verbose_name_plural = 'Фильмы'


"""class MovieDirector(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    director = models.ForeignKey(Director, on_delete=models.CASCADE)
    class Meta:
        verbose_name = 'Связь фильм <-> режиссер'
        verbose_name_plural = 'Связи фильмы <-> режиссеры'"""
